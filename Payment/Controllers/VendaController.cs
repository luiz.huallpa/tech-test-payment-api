using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Payment.Models;
using Payment.Context;
using Models.Enums;
using Microsoft.EntityFrameworkCore;

namespace Payment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        //   - A API deve possuir 3 operações:

        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;

        }

        //   2) Buscar venda: Busca pelo Id da venda;
        [HttpGet("ObterPorId{id}")]
        public async Task<ActionResult<Venda>> ObterPorId(int id)
        {

            var venda = await _context.Vendas.FindAsync(id);
            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        //   1) Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
        [HttpPost("Criar")]
        public async Task<ActionResult<Venda>> Criar(Venda venda)
        {
            if (venda.Items == null)
                return BadRequest(new { Erro = "A inclusão de uma venda deve possuir pelo menos 1 item" });

            venda.Status = EnumStatusVenda.AguardandoPagamento;
            _context.Add(venda);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }


        //   3) Atualizar venda: Permite que seja atualizado o status da venda.
        [HttpPut("AvancarStatus{id}")]
        public async Task<ActionResult<Venda>> Atualizar(int id, Venda venda)
        {
            var vendaBanco = await _context.Vendas.FindAsync(id);

            if (vendaBanco == null)
                return NotFound();

            var status = venda.Status;

            if (status != EnumStatusVenda.Cancelada && status != EnumStatusVenda.Entregue)
            {
                status = status + 1;
            }
            else if (status == EnumStatusVenda.Cancelada)
            {
                return BadRequest(new { Erro = "Não é possível avançar o status do pedido, esse pedido já foi cancelado" });
            }
            else if (status == EnumStatusVenda.Entregue)
            {
                return BadRequest(new { Erro = "Não é possível avançar o status do pedido, esse pedido já foi entregue" });
            }


            vendaBanco.Status = status;


            _context.Vendas.Update(vendaBanco);
            await _context.SaveChangesAsync();
            return Ok(vendaBanco);
        }

        [HttpPut("CancelarPedido{id}")]
        public async Task<ActionResult<Venda>> Cancelar(int id, Venda venda)
        {
            var vendaBanco = await _context.Vendas.FindAsync(id);

            if (vendaBanco == null)
                return NotFound();

            var status = venda.Status;

            if (status != EnumStatusVenda.Entregue)
                status = EnumStatusVenda.Cancelada;
            else
                return BadRequest(new { Erro = "Não é possível cancelar o pedido, esse pedido já foi entregue" });


            vendaBanco.Status = status;


            _context.Vendas.Update(vendaBanco);
            await _context.SaveChangesAsync();
            return vendaBanco;
        }




    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models.Enums;
using System.ComponentModel.DataAnnotations;


namespace Payment.Models
{

    public class Item
    {  
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }

    }
}
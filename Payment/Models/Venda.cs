using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace Payment.Models
{

    //Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
    public class Venda
    {   
        public int Id { get; set; }
        public Vendedor DadosVendedor { get; set; }
        public List<Item> Items { get; set; }
        public EnumStatusVenda Status { get; set; }
    }


}
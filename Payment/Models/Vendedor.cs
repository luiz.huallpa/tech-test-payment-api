using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Payment.Models
{
    public class Vendedor
    {
        //- O vendedor deve possuir id, cpf, nome, e-mail e telefone;
        [Key]
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }  
        public string Telefone { get; set; }
    }
}